const{createServer} = require('http');

let server = createServer(
    (request,response) => {
        response.writeHead(200 , {"content-type":"text/html"});
        response.write("<h1>Hello Wolrd!</h1>");
        response.end();
    }
);

server.listen(8000);

console.log("Projeto inciado na porta 8000");